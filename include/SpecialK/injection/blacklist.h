const
std::unordered_set <std::wstring> __blacklist = {
  L"steamwebhelper.exe",
  L"streaming_client.exe",
  L"steam.exe",
  L"html5app_steam.exe",
  L"wow_helper.exe",
  L"uninstall.exe",
  
  L"writeminidump.exe",
  L"crashreporter.exe",
  L"supporttool.exe",
  L"crashsender1400.exe",
  L"firaxisbugreporter.exe",
  
  L"dxsetup.exe",
  L"setup.exe",
  L"vc_redist.x64.exe",
  L"vc_redist.x86.exe"  
  L"vc2010redist_x64.exe",
  L"vc2010redist_x86.exe",
  L"vcredist_x64.exe",
  L"vcredist_x86.exe",
  L"ndp451-kb2872776-x86-x64-allos-enu.exe",
  L"dotnetfx35.exe",
  L"dotnetfx35client.exe",
  L"dotnetfx40_full_x86_x64.exe",
  L"dotnetfx40_client_x86_x64.exe",
  L"oalinst.exe",
  L"easyanticheat_setup.exe",
  L"uplayinstaller.exe",
  
  L"x64launcher.exe",
  L"x86launcher.exe",
  L"launcher.exe",
  L"ffx&x-2_launcher.exe",
  L"fallout4launcher.exe",
  L"skyrimselauncher.exe",
  L"modlauncher.exe",
  L"akibauu_config.exe",
  L"obduction.exe",
  L"grandia2launcher.exe",
  L"ffxiii2launcher.exe",
  L"bethesda.net_launcher.exe",
  L"ubisoftgamelauncher.exe",
  L"ubisoftgamelauncher64.exe",
  L"splashscreen.exe",
  L"gamelaunchercefchildprocess.exe",
  L"launchpad.exe",
  L"cnnlauncher.exe",
  L"ff9_launcher.exe",
  L"a17config.exe",
  L"a18config.exe", // Atelier Firis
  L"dplauncher.exe",
  L"zeroescape-launcher.exe",
  L"gtavlauncher.exe",
  L"gtavlanguageselect.exe",
  L"nioh_launcher.exe",
  L"rottlauncher.exe",
  L"configtool.exe",

  L"coherentui_host.exe",
  L"vhui64.exe",
  L"vhui.exe",
  L"activationui.exe",
  L"zossteamstarter.exe",
  L"eac.exe",
  L"devenv.exe",
  L"vcpkgsrv.exe",

  L"nvcontainer.exe",
  L"nvdisplay.container.exe",
  
  L"gameserver.exe",// Sacred   game server
  L"s2gs.exe",      // Sacred 2 game server
  
  L"launchtm.exe",

  L"activate.exe",
  L"clmpsvc.exe",
  L"clmshardwaretranscode.exe",
  L"clupdater.exe",

  L"olrstatecheck.exe",
  L"olrsubmission.exe",

  L"pdvdlp.exe",

  L"powerdvd.exe",
  L"powerdvdmovie.exe",
  L"powerdvd_help.exe",
  L"powerdvd17agent.exe",
  L"powerdvd17ml.exe",
  L"powerdvd17movie.exe",

  L"ism2.exe",
  L"chrome.exe",
  L"perfwatson2.exe",

  L"browser_broker.exe",
  L"microsoftedge.exe",
  L"microsoftedgecp.exe",

  L"shellexperiencehost.exe",

  L"conhost.exe", L"svchost.exe",

  L"explorer.exe",
  L"scriptedsandbox.exe",
  L"scriptedsandbox64.exe",
  L"nvidia web helper.exe",
};